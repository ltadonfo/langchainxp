bc3 = pd.read_csv('../../Data/BC3/BC3_UpdatedWithNewAnnotations_FBg_Tad_07_03_2023.csv', sep=';', encoding='utf-8')
    bc3 = bc3[bc3.Id.notnull()]
    # bc3['gpt3.5_turbo'] = None
    bc3.insert(7, "speech_act_from_gpt_2", None)
    bc3.insert(8, "gpt_sent_2", None)

    res = """"{
        "Email_1": {
            "inform": "hi all, As outlined at the face to face meet, I've been working on methods of getting coordinate information about the locations of Semantic web researchers and groups."
            "inform": "I wrote to the www-rdf-interest list asking for data: so we'll have to see how that pans out."
            "request": "Please do add yourselves if you've not already there (I just did a few as examples)."
            "inform": "There are some SVG demos, developed from ones by Jim Ley, displaying the information we have already, which I constructed as examples:"
            "inform": "So, now we have to decide on the map which we will use to overlay the dots on."
            "inform": "We want something that will highlight Europe, especially since on a world map, Europe is too small to really show up on provide the level of detail we require."
            "inform": "At ILRT, Caroline Meek, Dan and I have had discussions with a designer, and he has pointed us at some examples, and we've narrowed it down to two that we rather like:"
            "inform": "which has the advantage of showing Europe as part of the world (we could get rid of the arm of the globe that is showing, and alter the colours);"
            "inform": "which would be technically simpler to do, but shows Europe in isolation (I do like the colour scheme though)."
            "question": "Do people have a preference for one style over the other?"
            "request": "Could you let us know today if possible?"
            "inform": "We need to specify the map as soon as possible in order to get postcards printed for www2003."
            "thanks": "thanks"
            "politeness": "p.s."
            "inform": "Here's a nice similar idea: http://www.asemantics.net/showcase/zoom.html"
        },
        "Email_2": {
            "hypothesis": "Can't say I'm particularly grabbed by either of them, but if you have to pick one I'd go for the map, not the globe."
            "question": "Is there any reason it has to be geographically accurate?"
            "suggestion": "Perhaps a schematic, analogous to the London Underground map of London, would allow you to convey the sense of spatial distribution without getting hung up on the scale problem."
            "politeness": "Of course, you may not consider that a helpful suggestion given the short timescale :-) Cheers,"
        },
        "Email_3": {
            "inform": "One advantage of being geographically accurate is it simplifies our lives when we try to plot SW researchers, groups, events etc on the map..."
            "confirm": "I prefer the map to the globe, fwiw."
        },
        "Email_4": {
            "inform": "yeah, sorry it's such a rush."
            "explanation": "It took a long time to write the document explaining how to write the RDF."
            "inform": "At this stage we really need examples to show the designer though - I'll pass your comments along to him, see if he has anything like that."
            "question": "What don't you like about them in particular?"
            "thanks": "thanks for the feedback"
        },
        "Email_5": {
            "inform": "Hi Libby,"
            "disapprove": "The globe is too visually busy for a background (imho), and if you decrease the opacity I think you'll lose too much of the important bits (like where the UK is :-)."
            "inform": "The map would be OK as a backdrop for the FOAF map if it was semi-opaque, but (a) it misses out the rest of the world, as you said yourself, and (b) there's a big blue empty space on the left :-)."
            "explanation": "The atlantic is just too big** if you keep it to scale, hence my suggestion to use more of a schematic approach."
            "suggestion": "I guess you need the cartographical equivalent of an ellipsis, whatever that might be!"
            "politeness": "Cheers,"
        },
        "Email_6": {
            "thanks": "thanks Ian,"
            "confirm": "I broadly agree with you."
            "explanation": "For (b) though, that might work in our favour, as we can put a logo and/or a slogan there."
        }
    }"""
    try:
        insert_bc3_with_gtp3_speech_act_2(2, res)
        logging.info("Insert: YESSS")
    except Exception as e:
        print(e)
        logging.info("Insert: NOOOOO")
    logging.info(50 * '*')
    exit()


speechacts = """{
                    "Email_1": {
                        "politeness": "Hello Education and Outreach Colleagues,",
                        "suggestion": "How about we create a user-friendly version of the Web Content guidelines?",
                        "suggestion": "Maybe a primer.",
                        "inform": "I was talking to a friend today -- an amateur web developer and professional disability rights advocate -- who complained that the W3C guidelines are overly technical for her needs.",
                        "inform": "She wants a plain language version of the guidelines.",
                        "inform": "As she is fairly technologically savvy, she expressed frustration at having to work so hard to understand what must be done to make accessible web pages.",
                        "inform": "To illustrate her point, she read me the Quick Tip card description of Image map.",
                        "inform": "I agree with her, the tone is definitely geeky.",
                        "inform": "But not everyone who develops web pages speaks the language of client-side servers and hotspots.",
                        "inform": "I would guess that most people who develop web pages are amateurs (in the original sense of the word: from amore or amour: an activity done out of love.)",
                        "question": "Will these people freeze when they read make line by line reading sensible? or Use CSS? "
                    },
                    "Email_2": {
                        "inform": "you are not the first to ask about this...",
                        "inform":"this si a part of obtaining buy-in from the user community so implementation will go easier.",
                        "inform":"Remember my comments from toronto last year, create a wcag for dummies version....",
                        "question": "hey, are there going to be EO, WAI and WCAG meetings at the www9?"
                    },
                    "Email_3": {
                        "appreciation": "WL: Great idea.",
                        "request": "Would you take that as an action item?"
                    },
                    "Email_4": {
                        "inform": "What about the WCAG Curriculum?",
                        "inform": "Particularly the Example Set of the WCAG Curriculum?",
                        "inform":"Please note that the curriculum will be moving to W3C space in the next week or so.",
                        "inform":"Don't bookmark pages in the above site as they will soon be invalid.",
                        "inform":"Once the curriculum is in the W3C space, WAI EO will likely start promoting it more heavily.",
                        "politeness": "Regards,"
                    },
                    "Email_5": {
                        "request": "Please take a look at www.peepo.com/access for a first small attempt at this.",
                        "request": "If you are prepared to create something else, please try to use good examples rather than text. (as in QED) and post the URI so we can take a look."
                    },
                    "Email_6": {
                        "disapprove": "Got a could not connect to remote server from both links at http://www.peepo.com/access"
                    }
                }"""


'''
    thread_id, emails, already_load = load_enron_conversation([])
    test = ""
    for index, email in enumerate(emails):
        # inputs["Email"+str(index+1)] = email.split(':', 1)[1]
        # inputs.append({"Email"+str(index +1) + " :"+email.split(':', 1)[1])
        # inputs += "Email" + str(index+1) + "  " + str(email.split(':', 1)[1])+"\n\n"
        test += "[" + ' '.join(email.split(':', 1)[1].split()) + "]"
    '''
    print