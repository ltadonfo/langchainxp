import json

import pandas as pd
import random
#from transformers import OpenAIGPTTokenizer, OpenAIGPTModel
#import torch

#already_load = []

disclaimers = ['Confidentiality and/or Privilege Notice',
               'This message and any attachment are confidential',
               'This e-mail is the property of Enron Corp',
               'This message (including any attachments) contains confidential information'
              ]


def load_enron_conversation_for_utterances_pairing(already_load, seed):
    print



def load_conversation_from_bc3_for_utterances_pairing(bc3, already_load, for_speech_acts=False):
    thread_ids = list(set(bc3['Id'].tolist()))
    thread_ids_annotated = [10, 11, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
    #thread_ids_annotated = [15, 24]
    if for_speech_acts is True:
        thread_ids_annotated.append(12)
    else:
        thread_ids_annotated.append(1)
    bc3["thread_ID"] = bc3['Id'].apply(lambda x: int(str(x).split('.')[0]))
    bc3 = bc3.query('thread_ID in @thread_ids_annotated')
    while True:
        #seed += 1
        #random.seed(a=seed)
        thread_id = random.choice(thread_ids_annotated)
        if thread_id not in already_load:
            already_load.append(thread_id)
            conv = bc3.query(
                "thread_ID == @thread_id"
            ).reset_index(drop=True)
            break
        else:
          continue
    emails = []
    content, sender, rang = [], "",  1
    for index, email in conv.iterrows():
        nb = int(str(email['Id']).split('.')[2])
        if nb == 1:
            if len(content) > 9:
                temp = content[1: (len(content) - 10)+2]
                content[1:(len(content) - 10)+2] = []
                content.extend(temp)
            content = " ".join(content)
            if for_speech_acts is True:
                emails.append(content)
            else:
                emails.append({
                    'sender': email['Sender'],
                    'body': content
                })
            content =[]
            content.append(email['Item'])
        else:
            rang += 1
            content.append(email['Item'])
        if index == len(conv) -1:
            if len(content) > 9:
                temp = content[1: (len(content) - 10) + 2]
                content[1:(len(content) - 10) + 2] = []
                content.extend(temp)
            content = " ".join(content)
            if for_speech_acts is True:
                emails.append(content)
            else:
                emails.append({
                    'sender': email['Sender'],
                    'body': content
                })
        #if rang >= 1:
        #    emails[sender] = content


    if for_speech_acts is True:
        #emails = ' '.join(['[' + value + ']' for value in emails[1:]])
        tmp = {}
        for idx, email in enumerate(emails[1:]):
            tmp['Email_' + str(idx + 1)] = email
        emails = tmp

    else:
        emails = [{"email_" + str(index + 1): value
        } for index, value in enumerate(emails)]
        emails = {"conversation": emails}
    #return thread_id, json.dumps(emails), already_load
    return thread_id, emails, already_load



def load_enron_conversation(already_load):
    enron = pd.read_csv('../../Data/EnronThreadsCorpus/Processed/bin1/7018_21053_bin1_data_2022_09_08-15_14_15.csv', sep=';', encoding='utf-8')
    #enron['n_sent'] = enron['body'].apply(lambda x: len(str(x).split('\n')))
    thread_ids = list(set(enron['thread_ID'].tolist()))
    while True:
        #seed +=1
        #random.seed(a=seed)
        thread_id = random.choice(thread_ids)
        if thread_id not in already_load:
            already_load.append(thread_id)
            conv = enron.query(
                "thread_ID == @thread_id"
            ).reset_index(drop=True)
            nan_conv = conv[conv['body'].isnull()]
            sentences = [len(str(val).split('\n')) for val in conv['body'].tolist()]
            if len(nan_conv) + 1 == len(conv) or max(sentences) >15:
                continue
            else:
                break
    emails = []
    conv.sort_values('position')
    for index, email in conv.iterrows():
        print(type(email))
        body = email['body']
        #for disclaimer in disclaimers:
        #   body = body.split(disclaimer)[0]
        if str(body) == 'nan':
            continue
        else:
            emails.append("'Email " + str(index + 1) + "': " + str(body))
    if len(emails) == 0:
        load_enron_conversation(already_load)
    print(f"{thread_id} ***** {emails}")
    return thread_id, emails, already_load

'''
if __name__ == '__main__':
    thread_id, emails = load_enron_conversation([], 23)
    print

'''
