import logging
import os
import re

from langchain import LLMChain
from langchain.prompts.prompt import PromptTemplate
from langchain.prompts import load_prompt
from langchain.prompts.few_shot import FewShotPromptTemplate
import more_itertools
from thefuzz import fuzz

from load_conversations import *
from difflib import get_close_matches

os.environ['OPENAI_API_KEY'] = 'your_key'
os.environ['HUGGINGFACEHUB_API_TOKEN'] = 'our_key'


from langchain.llms import OpenAI
from langchain.chains import LLMChain

#model = 'gpt-3'
model ='davinci'
if model == 'gpt-3':
    llm = OpenAI(model_name='gpt-3.5-turbo')
else:
    llm = OpenAI(model_name='text-davinci-002', max_tokens=1784)

use_our_speech_acts = False

logs_file = '../data/for_speech_acts/gpt3_5turbo_results_6.log'

logging.basicConfig(filename=logs_file, filemode='a',
                    format="%(asctime)s, %(msecs)d %(name)s %(levelname)s [ %(filename)s-%(module)s-%(lineno)d ]  : %(message)s",
                    datefmt="%H:%M:%S",
                    level=logging.INFO)

examples = [
    {
        "input": """{
          "Email_1": ": "The IETF meetings tend to become too large, creating logistics and planning problems. I suggest that future meetings are held for two weeks, with applications and user services issues the first week, and all other issues the second week. Those who so wish could attend both weeks, and other people could attend only one week. Those who choose to attend both weeks would be able to cover more groups and do better liaisons between the different areas. The Friday of the first week could discuss applications issues which might be of special interest to the other areas, and the Monday of the second week would schedule other groups which might be of special interest to applications people, so some people could attend Monday-Monday or Friday-Friday.",
          "Email_2": "Terry;My problem over the past year or so is that there are only a few session I wish to attend, but I cannot know for sure when they will be scheduled, so I cannot make reasonable travel arrangements (a week in Orlando for 6 hours of meetings is hard to sell to management). Now I know there is a rationale here, and that one is encouraged to participate broadly., And I am hopeful that new activities (my own and in the IETF) will give me many more reasons to attend. But firmer scheduling would be a big win. regards,",
          "Email_3": "Terry, WG Chairs already are asked to specify which clashes they want to avoid, but clash avoidance is an iterative problem. I just don't see how late changes can be completely avoided. Obviously, everybody knows they need to be minimised. Jacob, No way. Taking one week out of our calendars 3 times a year is already very painful, and a lot of people really do need and want to track multiple areas of the IETF. Spreading it thinner would be awful.",
          "Email_4": "Working groups don't seem to decide at the last minute whether or not they need a meeting, or what their conflict avoidance criteria needs to be.This leads me to believe that we could create the IETF schedule earlier without causing significant pain. For example, it would be very useful so that the final WG schedule was available more than 21 days in advance, in order to get advanced purchase tickets. This would mean that the apps area schedule should be set tentatively 30-35 days in advance, so that the draft IETF schedule could be published and conflicts reviewed.",
          "Email_5": "I'd like to second this notion. Many folks in smaller companies are under severe budget constraints and have difficulty justifying participation in IETF meetings."
        }
        """,
        "output": """{
                "Email_1": {
                    "inform": "The IETF meetings tend to become too large, creating logistics and planning problems.",
                    "suggestion": "I suggest that future meetings are held for two weeks, with applications and user services issues the first week, and all other issues the second week.",
                    "suggestion": "Those who so wish could attend both weeks, and other people could attend only one week.",
                    "inform": "Those who choose to attend both weeks would be able to cover more groups and do better liaisons between the different areas.",
                    "suggestion": "The Friday of the first week could discuss applications issues which might be of special interest to the other areas, and the Monday of the second week would schedule other groups which might be of special interest to applications people, so some people could attend Monday-Monday or Friday-Friday.",
                },
                "Email_2": {
                    "inform": "My problem over the past year or so is that there are only a few session I wish to attend, but I cannot know for sure when they will be scheduled, so I cannot make reasonable travel arrangements (a week in Orlando for 6 hours of meetings is hard to sell to management).",
                    "inform": "Now I know there is a rationale here, and that one is encouraged to participate broadly.",
                    "inform":"And I am hopeful that new activities (my own and in the IETF) will give me many more reasons to attend.",
                    "inform": "But firmer scheduling would be a big win.",
                    "politeness": "regards"
                },
                "Email_3": {
                    "inform": "Terry, WG Chairs already are asked to specify which clashes they want to avoid, but clash avoidance is an iterative problem.",
                    "inform": "I just don't see how late changes can be completely avoided.",
                    "inform":"Obviously, everybody knows they need to be minimised.",
                    "disagreement": "Jacob, No way.",
                    "explanation":"Taking one week out of our calendars 3 times a year is already very painful, and a lot of people really do need and want to track multiple areas of the IETF.",
                    "infrom": "Spreading it thinner would be awful."
                },
                "Email_4": {
                    "inform": "Working groups don't seem to decide at the last minute whether or not they need a meeting, or what their conflict avoidance criteria needs to be.",
                    "suggestion": "This leads me to believe that we could create the IETF schedule earlier without causing significant pain.",
                    "suggestion": "For example, it would be very useful so that the final WG schedule was available more than 21 days in advance, in order to get advanced purchase tickets.",
                    "suggestion":"This would mean that the apps area schedule should be set tentatively 30-35 days in advance, so that the draft IETF schedule could be published and conflicts reviewed."
                    "disagreement": "Jacob, No way."
                    "explanation":"Taking one week out of our calendars 3 times a year is already very painful, and a lot of people really do need and want to track multiple areas of the IETF."
                    "infrom": "Spreading it thinner would be awful."
                },
                "Email_5": {
                    "appreciation": "I'd like to second this notion.",
                    "inform": "ThiMany folks in smaller companies are under severe budget constraints and have difficulty justifying participation in IETF meetings.",
                    "inform": "The advance notice helps reduce travel costs, which in turn helps more people attend."
                }
            }"""
    }
]




speech_acts = {'appreciation': 'AA', 'request responding': "AdR", 'addressing a suggestion': 'AdS', 'agreement': 'A', 'suggestion': 'S', 'answer': 'An', 'confirm': 'Cf', 'disagreement': 'Dag', 'disapprove': 'Dcf', 'explanation': 'Ex', 'hypothesis': 'Hy', 'topic change': 'Tpc', 'inform': 'I', 'instruct': 'Ist', 'offer': 'O', 'politeness': 'P', 'promise': 'Pr', 'question': 'Q', 'request': 'R'}
test = None
bc3= None


def insert_bc3_with_gtp3_speech_act_2(conversation_id, gpt_result, speech_act_from_gpt =False):
    global bc3
    gpt_result = gpt_result.splitlines()
    gpt_result = "\n".join([val.strip() for index, val in enumerate(gpt_result) if ((index not in [0, len(gpt_result) - 1]) and (not val.endswith(('{'))) and (not val.startswith('},')))])
    gpt_result = gpt_result.split('},')
    for email_number, email in enumerate(gpt_result):
        email = email.splitlines()
        email_number +=1
        n_sentences = len(email)
        sentence_number, i = 0, 0
        for sentence in email:
            if len(sentence)<2:
                continue
            sentence_number += 1
            sentence = sentence.split(':', 1)
            speech_act, sentence = sentence[0].replace('"', ""), sentence[1].strip()[1:][:-1]
            get_close_sentence(sentence, conversation_id, speech_act)
        print
    suffix = 'GPT' if model == 'gpt-3' else 'DVC'
    bc3.to_csv('../../Data/BC3/BC3_UpdatedWithNewAnnotations_FBg_Tad_' + suffix + '_' + in_name_file + '_08_05_2023.csv', sep=';', encoding='utf-8')


def get_close_sentence(sentence, thread_id, speech_act):
    bc3["thread_ID"] = bc3['Id'].apply(lambda x: int(str(x).split('.')[0]))
    conv = bc3.query(
                "thread_ID == @thread_id"
            ).reset_index(drop=True)
    conv['closed'] = conv['Item'].apply(lambda x: fuzz.ratio(sentence, x))
    conv = conv.sort_values(by=['closed'], ascending=False)
    found = conv.iloc[0]
    in_bc3 = bc3.query(
                "Id == @found['Id']"
            ).reset_index(drop=True).iloc[0]
    if in_bc3['speech_act_from_gpt_2'] is None and found['closed']> 50:
        if len(get_close_matches(speech_act, list(speech_acts.keys()))) > 0 :
            bc3['speech_act_from_gpt_2'].mask(bc3['Id'] == found['Id'], speech_acts[get_close_matches(speech_act, list(speech_acts.keys()))[0]], inplace=True)
        else:
            bc3['speech_act_from_gpt_2'].mask(bc3['Id'] == found['Id'], speech_act)
        bc3['gpt_sent_2'].mask(bc3['Id'] == found['Id'], sentence, inplace=True)
    else:
        print

def insert_bc3_with_gpt3_speech_act(conversation_id, gpt_result):
    gpt_result = json.loads(gpt_result, object_pairs_hook=dict_raise_on_duplicates, strict=False)
    global bc3
    email_number = 0
    for email in gpt_result.values():
        email_number += 1
        n_sentences = 0
        for key, sentences in email.items():
            if type(sentences) == str:
                n_sentences += 1
                email[key]=[sentences]
            elif type(sentences) == list:
                n_sentences += len(sentences)
        sentence_number, i = 0, 0
        for key, sentences in email.items():
            for sentence in sentences:
                if n_sentences > 9 and sentence_number == 1 and i < n_sentences:
                    for i in range(10, n_sentences+1):
                        sentence_id = str(conversation_id) + '.' + str(email_number) + '.' + str(i)
                        bc3['gpt3.5_turbo'].mask(bc3['Id'] == sentence_id, speech_acts[get_close_matches(key, list(speech_acts.keys()))[0]], inplace=True)
                        sentence = sentence if i == 10 else sentences[i-10]
                        bc3['gpt_sent'].mask(bc3['Id'] == sentence_id, sentence, inplace=True)
                    break
                else:
                    sentence_number += 1
                    sentence_id = str(conversation_id) + '.' + str(email_number) + '.' + str(sentence_number)
                    bc3['sgpt3.5_turbo'].mask(bc3['Id'] == sentence_id, speech_acts[get_close_matches(key, list(speech_acts.keys()))[0]], inplace=True)
                    bc3['gpt_sent'].mask(bc3['Id'] == sentence_id, sentence, inplace=True)
                print
    #bc3.to_csv('../../Data/BC3/BC3_UpdatedWithNewAnnotations_FBg_Tad_Gpt_05_05_2023.csv', sep=';', encoding='utf-8')
    print

def convert(tup, di):
    di = dict(tup)
    return di

if use_our_speech_acts is True:
    prefix = "Given the following emails list of a conversation, for each email, split the email's body into sentences while removing signature and the disclaimer from each email, and then return those sentences  with their respective speech acts from the following list: " + str(list(speech_acts.keys()))
    in_name_file = 'our_speech_acts'
else:
    in_name_file = 'generated_speech_acts'
    prefix ="Given the following emails list of a conversation, for each email, split the email's body into sentences while removing signature and the disclaimer from each email, and then return those sentences with your own respective speech acts"
def compute_speech_acts_with_gpt3(prompt_sample, already_load):
    while len(already_load) < 21:
        thread_id, content_input, already_load = load_conversation_from_bc3_for_utterances_pairing(bc3, already_load, for_speech_acts=True)
        print(f"Handled conversation: {thread_id}")
        try:
            langchain_request(prompt_sample, thread_id, content_input)
        except Exception as s:
            print(str(s))
            first_part = convert(more_itertools.take(len(content_input) // 2, content_input.items()), {})
            rest = {content[0]: content[1] for content in content_input.items() if content[0] not in first_part.keys()}
            for index, elt in enumerate([first_part, rest]):
                langchain_request(prompt_sample, thread_id, elt, part=str(index+1))


def langchain_request(prompt_sample, thread_id, input_content, part=None):
    speech_acts_prompt = FewShotPromptTemplate(
        examples=examples,
        example_prompt=prompt_sample,
        prefix=f"{prefix}",
        suffix=f"Input: {input_content}\nOutput:",
        template_format='jinja2',
        input_variables=["test"],
    )
    sentences_extraction_chain = LLMChain(llm=llm, prompt=speech_acts_prompt)
    # return sentences_extraction_chain.run(inputs)
    results = sentences_extraction_chain.run({'test': input_content})
    logging.info({
        'thread_id': str(thread_id) + ('' if part is None else '_' + part),
        'Input': input_content,
        'Output': results
    })
    try:
        insert_bc3_with_gtp3_speech_act_2(thread_id, results)
        logging.info("Insert: YESSS")
    except Exception as e:
        print(e)
        logging.info("Insert: NOOOOO")
    logging.info(50 * '*')


def dict_raise_on_duplicates(ordered_pairs):
    """Convert duplicate keys to JSON array."""
    d = {}
    for k, v in ordered_pairs:
        if k in d:
            if type(d[k]) is list:
                d[k].append(v)
            else:
                d[k] = [d[k],v]
        else:
           d[k] = v
    return d

if __name__ == '__main__':
    bc3 = pd.read_csv('../../Data/BC3/BC3_UpdatedWithNewAnnotations_FBg_Tad_07_03_2023.csv', sep=';', encoding='utf-8')
    bc3 = bc3[bc3.Id.notnull()]
    #bc3['gpt3.5_turbo'] = None

    bc3.insert(7, "speech_act_from_gpt_2", None)
    bc3.insert(8, "gpt_sent_2", None)

    example_formatter_template = """
        Input: {input}
        Output:{output}\n
        """
    example_prompt = PromptTemplate(input_variables=["input", "output"], template=example_formatter_template)
    #print(example_prompt.format(**examples[0]))
    compute_speech_acts_with_gpt3(example_prompt, [])
    exit()


