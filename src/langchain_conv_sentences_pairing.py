import logging
import openai

from langchain import LLMChain, HuggingFaceHub, HuggingFacePipeline
from langchain.prompts.prompt import PromptTemplate
from langchain.chains import LLMChain
from langchain.prompts.few_shot import FewShotPromptTemplate
from load_conversations import *
import os
os.environ['OPENAI_API_KEY'] = 'your_key'
os.environ['HUGGINGFACEHUB_API_TOKEN'] = 'our_key'

from transformers import AutoTokenizer, AutoModelForCausalLM, pipeline, AutoModelForSeq2SeqLM


'''
model_id = 'eachadea/legacy-ggml-vicuna-13b-4bit'# go for a smaller model if you dont have the VRAM
model = AutoModelForSeq2SeqLM.from_pretrained('../llama.cpp/models/ggml-vicuna-13b-4bit.bin')

pipe = pipeline(
    "text2text-generation",
    model=model,
    max_length=2000
)

llm = HuggingFacePipeline(pipeline=pipe)
'''



from langchain.llms import OpenAI


#model = 'gpt-3'
model ='davinci'
if model == 'gpt-3':
    llm = OpenAI(model_name='gpt-3.5-turbo')
else:
    llm = OpenAI(model_name='text-davinci-002', max_tokens=909)

use_our_speech_acts = False

logs_file = '../data/for_utters_pairing/davinci_results.log'

logging.basicConfig(filename=logs_file, filemode='a',
                    format="%(asctime)s, %(msecs)d %(name)s %(levelname)s [ %(filename)s-%(module)s-%(lineno)d ]  : %(message)s",
                    datefmt="%H:%M:%S",
                    level=logging.INFO)


examples = [
    {
         "input": """ { 
            "conversation": [
                "email_1": {
                    "sender":"Jutta",
                    "body":"Liddy has been admirably getting up at 2am on a regular basis to join our call. I don't think we should continue to ask her to do that. I would like to find an alternative time that works for everyone that regularly attends the teleconference. This needs to take into account European time, Australian time and both east and west coast North American time. How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday? This would make it 6:00am in Australia and 9:00 pm in Europe. Or does anyone have an alternative suggestion?",
                },
                "email_2": {
                    "sender":"Jan Richard",
                    "body":"According to the International Meeting Planner, Jutta's suggestion of 4pm EST is pretty good - although it is actually 10pm in Europe. 3pm EST would be 5am in Australia and 9pm in Europe."
                },
                "email_3": {
                    "sender":"Liddy",
                    "body":"Thank you; - I do appreciate this gesture. I believe 4.00pm on Tuesday would clash with the IMS meetings, if they continue at that time..."
                
                },
                "email_4": {
                    "sender":"Carlos",
                    "body":"Hi Jutta, The same issue arose a few weeks ago in the PF group It is very difficult to find a common time that works for every time zone. 10pm Central European Time means I can make some of the calls, but not all of them, but I understand the change. regards,"
                },
                "email_5": {
                    "sender":"Lou",
                    "body":"I hesitate to speak for Heather, but in her absence, the time is do-able here in Pacific time; however, at glancing at her schedule she may have conflicts with this time, at least on Mondays/Wednesdays --Tuesdays look like a 30 minute conflict (at the last 1/2 hour). I don't know if these are times that she can adjust or not. She'll be back and able to speak for her own schedule next week. When did you need to make a firm decision? Thanks!"
                },
                "email_6": {
                    "sender":"Liddy",
                    "body":"does it help to have different times? I will be in Europe for a few weeks soon and so we could have meeting times that suit the meetings - boring but maybe better for actual events?"
                },
                "email_7": {
                    "sender":"Doug",
                    "body":"I'm comfortable with whatever time is set and will remain flexible, even if it's 5:00am PDT."
                },
                "email_8": {
                    "sender":"Heather",
                    "body":"Monday, Tuesday, or Wednesday at 4:00 pm EST would work for me but Tuesday or Wednesday would be better. Fyi, I'm back but Lou Nell is going to sit in on the next call for me -- It's still Monday at 12:00 EST right?"
                }
            ]
       }""",
        "output": """
        {
             "Pair 1" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday?"
                }
                "From Email_2": {
                        "speech_act":"Answer",
                        "content": "According to the International Meeting Planner, Jutta's suggestion of 4pm EST is pretty good - although it is actually 10pm in Europe."
                }
            },
            "Pair 2" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "Or does anyone have an alternative suggestion?"
                }
                "From Email_2": {
                        "speech_act":"Answer",
                        "content": "According to the International Meeting Planner, Jutta's suggestion of 4pm EST is pretty good - although it is actually 10pm in Europe."
                }
            },
            "Pair 3" : {
                "From Email_1": {
                        "speech_act":"Explanation",
                        "content": "This would make it 6:00am in Australia and 9:00 pm in Europe."
                }
                "From Email_2": {
                        "speech_act":"Inform",
                        "content": "3pm EST would be 5am in Australia and 9pm in Europe."
                }
            },
            "Pair 4" : {
                "From Email_1": {
                        "speech_act":"Promise",
                        "content": "I would like to find an alternative time that works for everyone that regularly attends the teleconference."
                }
                "From Email_3": {
                        "speech_act":"Appreciation",
                        "content": "Thank you, - I do appreciate this gesture."
                }
            },
            "Pair 5" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday?"
                }
                "From Email_3": {
                        "speech_act":"Inform",
                        "content": "I believe 4.00pm on Tuesday would clash with the IMS meetings, if they continue at that time..."
                }
            },
            "Pair 6" : {
                "From Email_1": {
                        "speech_act":"Promise",
                        "content": "I would like to find an alternative time that works for everyone that regularly attends the teleconference."
                }
                "From Email_4": {
                        "speech_act":"Inform",
                        "content": "Hi Jutta, The same issue arose a few weeks ago in the PF group."
                }
            },
            "Pair 7" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday? This would make it 6:00am in Australia and 9:00 pm in Europe."
                }
                "From Email_4": {
                        "speech_act":"Answer",
                        "content": "10pm Central European Time means I can make some of the calls, but not all of them, but I understand the change."
                }
            },
            "Pair 8" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday?"
                }
                "From Email_5": {
                        "speech_act":"Agreement",
                        "content": "I hesitate to speak for Heather, but in her absence, the time is do-able here in Pacific time"
                }
            },
            "Pair 9" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday?"
                }
                "From Email_5": {
                        "speech_act":"Explanation",
                        "content": "however, at glancing at her schedule she may have conflicts with this time, at least on Mondays/Wednesdays;--Tuesdays look like a 30 minute conflict (at the last 1/2 hour). I don't know if these are times that she can adjust or not."
                }
            },
            "Pair 10" : {
                "From Email_4": {
                        "speech_act":"Inform",
                        "content": "Hi Jutta, The same issue arose a few weeks ago in the PF group."
                }
                "From Email_5": {
                        "speech_act":"Inform",
                        "content": "It is very difficult to find a common time that works for every time zone."
                }
            },
            "Pair 11" : {
                "From Email_2": {
                        "speech_act":"Inform",
                        "content": "3pm EST would be 5am in Australia and 9pm in Europe."
                }
                "From Email_6": {
                        "speech_act":"Question",
                        "content": "does it help to have different times?"
                },
            },
            "Pair 12" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday? This would make it 6:00am in Australia and 9:00 pm in Europe. Or does anyone have an alternative suggestion?"
                }
                "From Email_7": {
                        "speech_act":"Answer",
                        "content": "I'm comfortable with whatever time is set and will remain flexible, even if it's 5:00am PDT."
                },
            },
            "Pair 13" : {
                "From Email_1": {
                        "speech_act":"Question",
                        "content": "How is everyone with 4:00pm EDT on Monday, Tuesday or Wednesday? This would make it 6:00am in Australia and 9:00 pm in Europe. Or does anyone have an alternative suggestion?"
                }
                "From Email_8": {
                        "speech_act":"Answer",
                        "content": "Monday, Tuesday, or Wednesday at 4:00 pm EST would work for me but Tuesday or Wednesday would be better."
                },
            },
            "Pair 13" : {
                "From Email_5": {
                        "speech_act":"Inform",
                        "content": "She'll be back and able to speak for her own schedule next week."
                }
                "From Email_8": {
                        "speech_act":"Inform",
                        "content": "Fyi, I'm back but Lou Nell is going to sit in on the next call for me --"
                },
            }
        }"""
    }
]

speech_acts = {'appreciation': 'AA', 'request responding': "AdR", 'addressing a suggestion': 'AdS', 'agreement': 'A', 'suggestion': 'S', 'answer': 'An', 'confirm': 'Cf', 'disagreement': 'Dag', 'disapprove': 'Dcf', 'explanation': 'Ex', 'hypothesis': 'Hy', 'topic change': 'Tpc', 'inform': 'I', 'instruct': 'Ist', 'offer': 'O', 'politeness': 'P', 'promise': 'Pr', 'question': 'Q', 'request': 'R'}
test = None

if __name__ == '__main__':
    bc3 = pd.read_csv('../../Data/BC3/BC3_UpdatedWithNewAnnotations_FBg_Tad_07_03_2023.csv', sep=';', encoding='utf-8')
    bc3 = bc3[bc3.Id.notnull()]
    example_formatter_template = """
        Input: {input}
        Output:{output}\n
        """
    example_prompt = PromptTemplate(input_variables=["input", "output"], template=example_formatter_template)

    print(example_prompt.format(**examples[0]))
    thread_id, emails, already_load = load_conversation_from_bc3_for_utterances_pairing(bc3, [])
    prompt = FewShotPromptTemplate(
        examples=examples,
        example_prompt=example_prompt,
       # prefix=f"Given the following emails list  with their respective sender of a conversation, for each email, split the email's body without the signature nor the disclaimer into sentences and returns pairs of sentences from separate emails with the second element of the pair which  is a kind of answer to the first extract from a previous email and the elements of each pair must not come from the same emails.",
        #prefix=f"Given the following emails list of a conversation, for each email, split the email's body without the signature nor the disclaimer into sentences and use them to build pairs of sentences from separate emails. Sentences within each pair must not come from the same emails and the second sentence in the pair is related or is an answer to the first sentence in this pair.",
        #prefix=f"Given the following list of emails from a conversation, for each email, split his body into sentences and removing emails signatures and disclaimers. with those sentences, identify pairs of sentences that are related to each other in the input conversation. Each pair must contains two sentences, one from the sender of the email and the other from a different sender in response to that email. The pairs must be identified by comparing the context and content of each sentence and determining if they are related to the same topic or issue.",
        prefix=f"Given the following list of emails from a conversation, for each email, split his body into sentences while removing emails signatures and disclaimers. Identify pairs of those sentences that are related to each other in the input conversation. Each pair must contain two sentences, one from the sender of the email and the other from a different sender in response to that email. The pairs must be identified by comparing the context and content of each sentence and determining if they are related to the same topic or issue. Each sentence  within a pair must be label with his speech acts taken from this list: :{speech_acts}",
        #f" of sentences with each of the sentences in the pair coming from separate emails of the conversation and within each pair the second element in the pair must be an  answer or follows to the first one.",
        suffix=f"Input: {emails}\nOutput:",
        template_format='jinja2',
        input_variables=["emails", "speech_acts"],
    )


    print(prompt.format(emails=emails))
    sentences_extraction_chain = LLMChain(llm=llm, prompt=prompt)
    #return sentences_extraction_chain.run(inputs)
    results = sentences_extraction_chain.run({'emails': emails, 'speech_acts': list(speech_acts.keys())})
    print
