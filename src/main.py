# This is a sample Python script.
# Installez le client gpt-3-simple en utilisant pip
import os

os.environ['OPENAI_API_KEY'] = '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'
os.environ['HUGGINGFACEHUB_API_TOKEN'] = ''


from langchain import PromptTemplate

import openai


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    # Utilisez la fonction Completion.create de l'API de GPT-3 pour générer une conversation d'emails sur le sujet de la planification d'une réunion
    model_engine = "text-davinci-002"
    prompt = '''Bonjour,
    
    Je voulais savoir si vous seriez disponibles pour une réunion la semaine prochaine afin de discuter de la planification de notre prochain projet. Quels sont vos horaires de disponibilité ?
    
    Cordialement,
    Lionel'''
    completions = openai.Completion.create(
        engine=model_engine,
        prompt=prompt,
        max_tokens=1024,
        n=3,
        stop=None,
        temperature=0.7
    )

    # Affichez la conversation d'emails générée
    generated_conversation = completions.choices[0].text
    print(generated_conversation)
