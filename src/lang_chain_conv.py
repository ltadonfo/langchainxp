import os

from langchain import FewShotPromptTemplate

os.environ['OPENAI_API_KEY'] = 'your_key'
os.environ['HUGGINGFACEHUB_API_TOKEN'] = 'our_key'

from langchain.prompts import PromptTemplate
from langchain.llms import OpenAI
from langchain.chains import LLMChain

llm = OpenAI(model_name='gpt-3.5-turbo')

from load_conversations import *

already_load = []
speech_acts = {'aa': 'appreciation', 'adr': 'request responding', 'ads': 'addressing  a suggestion', 'ag': 'agreement', 's': 'suggestion',
                'an': 'answer', 'cf': 'confirm', 'dag': 'disagreement', 'dcf': 'disprove', 'ex': 'explanation', 'hy': 'hypothesis','tc': 'topic change',
                'i': 'inform', 'is': 'instruct', 'o': 'offer', 'p': 'politeness', 'pr': 'promise', 'q': 'question', 'r': 'request',
                 }


def emails_conv_few_shot_speech_acts(speech_acts_few_prompt, inputs):
    sentences_extraction_chain = LLMChain(llm=llm, prompt=speech_acts_few_prompt)
    return sentences_extraction_chain.run(inputs)


def create_corpus(emails):
    global already_load
    emails_sentences_template = """
    Given the following emails of a conversation:
    
    {emails}
    
    for each email, split the email's body without the signature nor the disclaimer into sentences and return list of sentences of each email
    """
    sentence_prompt = PromptTemplate(
        input_variables=["emails"],
        template=emails_sentences_template,
    )
    sentences_extraction_chain = LLMChain(llm=llm, prompt=sentence_prompt)
    sentences = sentences_extraction_chain.run(emails)

    speech_acts_template = """
        Using this list of dialogue acts : {speech_acts} and  assign a unique one to the following sentences and return them as JSON
        
        """ + sentences
    speech_acts_prompt = PromptTemplate(
        input_variables=['speech_acts'],
        template=speech_acts_template,
    )
    speech_acts_chain = LLMChain(llm=llm, prompt=speech_acts_prompt)
    answers = speech_acts_chain.run(list(speech_acts.values()))
    return sentences, answers




if __name__ == '__main__':
    """
     # First, create the list of few shot examples.
    examples = [
        {"word": "happy", "antonym": "sad"},
        {"word": "tall", "antonym": "short"},
    ]

    # Next, we specify the template to format the examples we have provided.
    # We use the `PromptTemplate` class for this.
    example_formatter_template = "
    Word: {word}
    Antonym: {antonym}\n
    "
    example_prompt = PromptTemplate(
        input_variables=["word", "antonym"],
        template=example_formatter_template,
    )

    # Finally, we create the `FewShotPromptTemplate` object.
    few_shot_prompt = FewShotPromptTemplate(
        # These are the examples we want to insert into the prompt.
        examples=examples,
        # This is how we want to format the examples when we insert them into the prompt.
        example_prompt=example_prompt,
        # The prefix is some text that goes before the examples in the prompt.
        # Usually, this consists of intructions.
        prefix="Give the antonym of every input",
        # The suffix is some text that goes after the examples in the prompt.
        # Usually, this is where the user input will go
        suffix="Word: {input}\nAntonym:",
        # The input variables are the variables that the overall prompt expects.
        input_variables=["input"],
        # The example_separator is the string we will use to join the prefix, examples, and suffix together with.
        example_separator="\n\n",
    )

    test ="big"
    # We can now generate a prompt using the `format` method.
    print(few_shot_prompt.format(input=test))
    """

    '''
     for i in range(5):
        thread_id, emails, already_load = load_enron_conversation([], 89+i)
        sentences, responses = create_corpus(emails)
    print()
    '''

