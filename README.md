# LangChainXp

In this project, [LangChain](https://python.langchain.com/en/latest/index.html) is used with OpenAi models (GPT3.5_turbo and InstructGPT-Davinci) to :
- [langchain_conv_speech_acts](src/langchain_conv_speech_acts.py): Segment emails conversation into sentences and label each of them with one of the following speech acts. 
```
speech_acts = {'appreciation': 'AA', 'request responding': "AdR", 'addressing a suggestion': 'AdS', 'agreement': 'A', 'suggestion': 'S', 'answer': 'An', 'confirm': 'Cf', 'disagreement': 'Dag', 'disapprove': 'Dcf', 'explanation': 'Ex', 'hypothesis': 'Hy', 'topic change': 'Tpc', 'inform': 'I', 'instruct': 'Ist', 'offer': 'O', 'politeness': 'P', 'promise': 'Pr', 'question': 'Q', 'request': 'R'}
```
- [langchain_conv_sentences_pairing](src/langchain_conv_sentences_pairing.py): Constitute pairs of sentences from email conversations while labelling each sentence with a speech act

